#!/bin/bash

PROTOC=./packages/grpc.tools/1.16.0/tools/linux_x64/protoc
PLUGIN=./packages/grpc.tools/1.16.0/tools/linux_x64/grpc_csharp_plugin
PROTOBUFDIR=./packages/google.protobuf.tools/3.6.1/tools
INDIR=../proto
OUTDIR=./generated

IFS=$'\n'; set -f
for f in $(find $INDIR -name '*.proto'); do
OUTPATH=$(dirname "$f")
OUTPATH=${OUTPATH#$INDIR/}
echo Compiling $f to $OUTDIR/$OUTPATH
mkdir -p $OUTDIR/$OUTPATH
$PROTOC -I$PROTOBUFDIR --proto_path=$INDIR --csharp_out=$OUTDIR/$OUTPATH --grpc_out=$OUTDIR/$OUTPATH $f --plugin=protoc-gen-grpc=$PLUGIN
done
unset IFS; set +f
