@echo off
setlocal enabledelayedexpansion

set PROTOC=%~dp0packages\grpc.tools\1.16.0\tools\windows_x64\protoc.exe
set PLUGIN=%~dp0packages\grpc.tools\1.16.0\tools\windows_x64\grpc_csharp_plugin.exe
set PROTOBUFDIR=%~dp0packages\google.protobuf.tools\3.6.1\tools
set INDIR=%~dp0proto
set OUTDIR=%~dp0generated

:: The protoc compiler has a lot of issues with file paths so a lot of stuff has to be done manually
:: The next lines calculate the length of the INDIR to be able to cut it from the full file path.
set cutter=%INDIR%
set cut=
:loop
if not "!cutter!"=="" (
    set /a cut += 1
    set cutter=!cutter:~1!
    goto :loop
)
set /a cut += 1

if not exist %OUTDIR% mkdir %OUTDIR%
for /R %INDIR% %%F in (*.proto) do (
    set relativefile=%%~fF
    set relativefile=!relativefile:~%cut%!
    set relativefile=!relativefile:\=/!
    set outpath=%%~dF%%~pF
    set outpath=!outpath:~%cut%!
    echo Compiling !relativefile! to !outpath!
    if not exist %OUTDIR%\!outpath! mkdir %OUTDIR%\!outpath!
    %PROTOC% -I%PROTOBUFDIR% --proto_path=%INDIR% --csharp_out=%OUTDIR% --csharp_opt=base_namespace= --grpc_out=%OUTDIR%\!outpath! !relativefile! --plugin=protoc-gen-grpc=%PLUGIN%
)